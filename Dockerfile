# How to create a basic docker image, with a receipe??

## Start from Ubuntu basic image 
FROM ubuntu:18.04                     

## Run two commands 
RUN apt-get update && \                  
	apt-get install -y samtools bwa 

## Run another
RUN apt-get install -y rna-star

